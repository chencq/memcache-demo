package cn.chencq.memcached.demo;

import java.util.Map;

public class Test {

	public static void main(String[] args) {

		// 得到MemcachedManager实例
		MemcachedManager cacheManager = MemcachedManager.getInstance();
		
		/*for (int i = 0; i < 2000; i++) {
			cacheManager.set("foo_"+i, "This is a string test.");
			Student student = new Student("chencq"+i, "男", 26+i);
			cacheManager.set("student_"+i, student);
		}*/
		
		//cacheManager.flushAll();

		String[] keys = { "student_18", "foo_13","student_181", "foo_131","student_128", "foo_133" };
		Map<String, Object> map = cacheManager.getMulti(keys);
		System.out.println(map.get("foo_13"));
		System.out.println(((Student) map.get("student_18")).getName());

	}
}
