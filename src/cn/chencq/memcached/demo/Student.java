package cn.chencq.memcached.demo;

import java.io.Serializable;

public class Student implements Serializable {

	private static final long serialVersionUID = -843032673582753779L;
	
	private String name;
	private String sex;
	private Integer age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Student(String name, String sex, Integer age) {
		super();
		this.name = name;
		this.sex = sex;
		this.age = age;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{\"name\":");
		builder.append(name);
		builder.append(", \n \"sex\":");
		builder.append(sex);
		builder.append(", \n \"age\":");
		builder.append(age);
		builder.append("\"\n}");
		return builder.toString();
	}

}
